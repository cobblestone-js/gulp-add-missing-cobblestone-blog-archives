var through = require("through2");
var path = require("path");
var ejs = require("ejs");
var fs = require("fs");
var grayMatter = require("gray-matter");
var Vinyl = require("vinyl");

module.exports = function(params)
{
    // Parse and figure out all the parameters.
    if (!params) params = {};
    params.relativePath = params.relativePath || "blog/";
    params.site = params.site || {};

    // We need to compile the template.
    var templateFilename = params.ejsTemplate;
    var template = fs.readFileSync(templateFilename).toString();
    var render = ejs.compile(template);

    // Set up the scanner that looks for existing categories but also figures
    // out which ones we'll need. In the finalize function, we'll create the
    // missing entries.
    var slugsSeen = {};
    var neededArchives = {};

    var pipe = through.obj(
        function(file, encoding, callback)
        {
            // Ignore the non-blog entries, this only handles number-based ones.
            // We search the relativePath because it makes it easier to not
            // include the full path searching.
            var slug = file.data.relativePath;

            if (!slug || !slug.startsWith(params.relativePath))
            {
                return callback(null, file);
            }

            // Remove the prefix and any leading directory separators. This will
            // give us a slug as a relative path inside the blog entries.
            slug = slug.replace(new RegExp("^" + params.relativePath + "/*"), "");

            // Keep trag of all the slugs we've seen.
            slugsSeen[slug] = true;

            // Loop through and make sure we have a "need" for every entry
            // above this blog post. We strip off the first file because that
            // is always the actual blog post and we know it is created.
            slug = path.dirname(slug);

            while (slug !== ".")
            {
                neededArchives[slug] = true;
                slug = path.dirname(slug);
            }

            // Return the file to let the pipe run through.
            return callback(null, file);
        },
        function(callback)
        {
            // Loop through all the needed archives to figure out which ones
            // have to be generated.
            for (var needed in neededArchives)
            {
                // If we've already seen it, then skip it.
                if (slugsSeen.hasOwnProperty(needed))
                {
                    continue;
                }

                // At this point, we need to create the file. We do this by
                // creating the file using the EJS template with the only
                // parameter (name) with the name.
                var name = path.basename(needed);
                var results = render({ name: name });

                // Pull out the YAML header so we can reconstruct what other
                // files have at this point.
                var matter = grayMatter(results);

                // Now that we have a file, we have to inject it
                // into the pipeline.
                var file = new Vinyl({
                    cwd: process.cwd(),
                    base: process.cwd(),
                    path: path.join(params.relativePath, needed, "index.html"),
                    contents: new Buffer(matter.content)
                });
                var relativePath = path.join(params.relativePath, needed) + "/";

                matter.data.relativePath = relativePath;
                file.page = matter.data;
                file.data = {
                    page: matter.data,
                    site: params.site,
                    relativePath: relativePath,
                    contents: matter.content
                };

                this.push(file);
            }

            // Finish processing the finalize.
            callback();
        });

    return pipe;
}
